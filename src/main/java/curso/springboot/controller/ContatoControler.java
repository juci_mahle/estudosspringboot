package curso.springboot.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import curso.springboot.model.Contato;
import curso.springboot.model.Pessoa;
import curso.springboot.repository.ContatoRepository;
import curso.springboot.repository.PessoaRepository;

@Controller
public class ContatoControler {

	@Autowired
	private ContatoRepository contatoRepository;
	
	@Autowired 
	private PessoaRepository pessoaRepository;
	
	@RequestMapping(method = RequestMethod.GET, value = "/cadastrocontatos")
	public ModelAndView inicio() {
		
		ModelAndView modelAndView = new ModelAndView("cadastro/cadastrocontatos");
		modelAndView.addObject("pessoaObj", new Pessoa());
		return modelAndView;
	}
	
	//@GetMapping("**/adicionarcontato/{pessoaid}")
	@RequestMapping(method = RequestMethod.POST, value = "**/salvarcontato")
	public ModelAndView salvar(Contato contato) {
		contatoRepository.save(contato);

		ModelAndView modelAndView = new ModelAndView("cadastro/cadastrocontatos");
		Iterable<Contato> contatoIt = contatoRepository.findAll();
		modelAndView.addObject("pessoaObj",  new Pessoa());
		modelAndView.addObject("contatoobj", contatoIt);
		return modelAndView;
	}

	
	@GetMapping("**/listarContatos/{idpessoa}")
	public ModelAndView listarContatos(@PathVariable("idpessoa") Long idpessoa) {
		
		Optional<Pessoa> pessoa = pessoaRepository.findById(idpessoa);
		
		ModelAndView modelAndView = new ModelAndView("cadastro/cadastrocontatos");
		modelAndView.addObject("pessoaobj", pessoa.get());
		modelAndView.addObject("contatos", contatoRepository.getContatos(idpessoa));
		return modelAndView;
	}
	
	@PostMapping("**/adicionarcontato/{pessoaid}")
	public ModelAndView adicionarcontato(Contato contato, @PathVariable("pessoaid") Long pessoaid) {
		
		Optional<Pessoa> pessoa = pessoaRepository.findById(pessoaid);
		contato.setPessoa(pessoa.get());
		
		if (contato != null && (contato.getNumero().isEmpty() || contato.getTipo().isEmpty())) {
			ModelAndView modelAndView = new ModelAndView("cadastro/cadastrocontatos");
			modelAndView.addObject("pessoaobj", pessoa.get());
			modelAndView.addObject("contatos", contatoRepository.getContatos(pessoaid));
			
			List<String> msg = new ArrayList<String>();
			if(contato.getNumero().isEmpty()) {
				msg.add("Número deve ser informado");
			}else if (contato.getTipo().isEmpty()) {
				msg.add("Número deve ser informado");
			}
			modelAndView.addObject("msg", msg); 
			return modelAndView;
		}
		
		contatoRepository.save(contato);
		
		ModelAndView modelAndView = new ModelAndView("cadastro/cadastrocontatos");
		modelAndView.addObject("pessoaobj", pessoa.get());
		modelAndView.addObject("contatos", contatoRepository.getContatos(pessoaid));
		return modelAndView;		
	}	
	
	@GetMapping("**/editarcontato/{idcontato}")
	public ModelAndView editar(@PathVariable("idcontato") Long idcontato) {
		ModelAndView modelAndView = new ModelAndView("cadastro/cadastrocontatos");
		Optional<Contato> contato = contatoRepository.findById(idcontato);
		
		modelAndView.addObject("pessoaObj", new Pessoa());
		modelAndView.addObject("contatosobj", contato.get());
		return modelAndView;
	}

	@GetMapping("**/excluircontato/{idcontato}")
	public ModelAndView excluir(@PathVariable("idcontato") long idcontato) {

		contatoRepository.deleteById(idcontato);

		ModelAndView modelAndView = new ModelAndView("cadastro/cadastrocontatos");
		modelAndView.addObject("pessoaObj",  new Pessoa());
		modelAndView.addObject("contatosobj",contatoRepository.findAll());
		return modelAndView;
	}
}