package curso.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import curso.springboot.model.Cidade;
import curso.springboot.repository.CidadeRepository;

@Controller
public class CidadeController {

	@Autowired
	private CidadeRepository cidadeRepository;
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/cadastrocidade")
	public String inicio() {
		return "cadastro/cadastrocidade";
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/salvarcidade")
	public String salvar(Cidade cidade) {
		cidadeRepository.save(cidade);
		
		/*ModelAndView andView = new ModelAndView("cadastro/cadastrocidade");
		Iterable<Cidade> cidadeIt = cidadeRepository.findAll();
		andView.addObject("cidades", cidadeIt);
		return andView;*/
		return "cadastro/cadastrocidade";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/listacidades")
	public ModelAndView listaCidade() {
		ModelAndView andView = new ModelAndView("cadastro/cadastrocidade");
		Iterable<Cidade> cidadeIt = cidadeRepository.findAll();
		andView.addObject("cidades", cidadeIt);
		return andView;		
	}	
}
