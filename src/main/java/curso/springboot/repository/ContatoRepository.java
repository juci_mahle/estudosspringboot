package curso.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import curso.springboot.model.Contato;

@Repository
@Transactional

public interface ContatoRepository extends CrudRepository<Contato, Long> {

	@Query("select c from Contato c where c.pessoa.id = ?1")
	public List<Contato> getContatos(Long pessoa_id);
	
}
